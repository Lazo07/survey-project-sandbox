package com.fujitsu.surveyproject.dao;

import java.util.List;

import com.fujitsu.surveyproject.model.User;

public interface UserDao {
	public User create(final User user);
	public List findAll();
	public User findUserById(int id);
}
