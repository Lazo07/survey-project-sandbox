package com.fujitsu.surveyproject.dao.implement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.fujitsu.surveyproject.dao.UserDao;
import com.fujitsu.surveyproject.model.User;

@Repository
public class UserDaoImpl implements UserDao {

	private final String INSERT_SQL = "INSERT INTO USERS(name,email) values(?,?)";
	private final String FETCH_SQL = "select id, name, email from users";
	private final String FETCH_SQL_BY_ID = "select * from users where id = ?";

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public User create(final User user) {
		KeyHolder holder = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(INSERT_SQL, Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, user.getName());
//				ps.setString(2, user.getAddress());
				ps.setString(3, user.getEmail());
				return ps;
			}
		}, holder);

		int newUserId = holder.getKey().intValue();
		user.setId(newUserId);
		return user;
	}

	public List findAll() {
		return jdbcTemplate.query(FETCH_SQL, new UserMapper());
	}

	public User findUserById(int id) {
		return (User) jdbcTemplate.queryForObject(FETCH_SQL_BY_ID, new Object[] { id }, new UserMapper());
	}

}

@SuppressWarnings("rawtypes")
class UserMapper implements RowMapper {

	@Override
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		User user = new User();
		user.setId(rs.getInt("id"));
		user.setName(rs.getString("name"));
		user.setEmail(rs.getString("email"));
		return user;
	}

}
