package com.fujitsu.surveyproject.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fujitsu.surveyproject.dao.implement.UserDaoImpl;

@Controller
@RequestMapping("/users")
public class MainController {
	private final UserDaoImpl user;
	
    @Autowired
    public MainController(UserDaoImpl user) {
        this.user = user;
    }

    @GetMapping("/list")
    public String showUpdateForm(Model model) {
    	System.out.println("code went here! == ");
        model.addAttribute("users", user.findAll());
        return "index";
    }
}
