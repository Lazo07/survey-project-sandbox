# Survey Project Sandbox

Testing grounds (sandbox) for survey app tech stack.

1. Create dummy schema in postgresql first then execute these create table and 
insert scripts:

CREATE TABLE users
(
    ID int NOT NULL ,
    name varchar(100) NOT NULL,
    email varchar(100) DEFAULT NULL,
    PRIMARY KEY (id)
);

insert into users(id, name, email) values(1,'Siva','siva@gmail.com');
insert into users(id, name, email) values(2,'Prasad','prasad@gmail.com');
insert into users(id, name, email) values(3,'Reddy','reddy@gmail.com');

2. Once tomcat is started, 
Access via this link to test jdbc connection : http://localhost:8080/users/list
